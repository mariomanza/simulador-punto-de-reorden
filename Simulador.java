import java.util.Random;

public class Simulador {

    public static int ciclos = 0;
    public static int reloj = -1;
    public static int stop = 0;
    public static int stock;
    public static int[] consumo;
    public static Negocio negocio;
    public static Random rnd = new Random();
    public String data = "";
    public int[] x_y;
    public static String status = "";

    public Simulador(int inv, int punto, int[] con, int[] tiempo, int dias) { // inicializador/constructor
        stop = dias;
        stock = inv;
        consumo = con;
        x_y = new int[dias+1];
        Producto producto = new Producto("GPU", stock, 1345, punto);
        negocio = new Negocio("Epic Ericktronics", tiempo, producto, rnd); // crear objeto Negocio
    }

    public String loop() {
        data = "";
        while (ciclos < stop) {
            reloj -= 1;
            ciclos += 1;

            data += "Dia "+ciclos+"\n";
            x_y[ciclos] = negocio.produ.cantidad;

            if (reloj == 0) {
                data += negocio.Recepcion(stock);
                reloj = -1;
            }

            data += negocio.Consumo(rnd.nextInt(consumo[1] - consumo[0]) + consumo[0]);

            status = negocio.CheckInventario();
            if (status.contains("Bajo")) {
                if (reloj > 0){
                    data += status;
                    data += "Pedido en camino. Llegada en "+reloj+" dias.\n";
                } else if (reloj < 0) {
                    reloj = negocio.Emision();
                }
            } else {
                data += status;
            }
        }

        return data;
    }
}