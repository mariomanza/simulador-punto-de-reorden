public class Producto {

    public String nombre;
    public int cantidad;
    public int precio;
    public int limite; //mìnimo	 

    public Producto(String n, int c, int p, int l) {
        nombre = n;
        cantidad = c;
        precio = p;
        limite = l;
    }
}