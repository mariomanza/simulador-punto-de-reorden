import java.util.Random;

public class Negocio {

    public String name;
    public Producto produ;
    public Random rnd;
    public int punto_reorden;
    public int quantity;
    public int[] time;

    public Negocio(String n, int[] tiempo, Producto p, Random rand) {
        name = n;
        produ = p;
        rnd = rand;
        this.time = tiempo;
    }

    public String Consumo(int cant) {
        if ((produ.cantidad - cant) <= 0) {
            return "Sin Producto Suficiente";
        } else {
            produ.cantidad -= cant;
            return "";
        }
    }

    public int Emision() {
        return rnd.nextInt(time[1] - time[0]) + time[0];
    }

    public String Recepcion(int cantidad) {
        produ.cantidad += cantidad;
        return "Producto recibido. Total: "+produ.cantidad+"\n";
    }

    public String CheckInventario() {
        if (produ.cantidad <= produ.limite) {
            return "Stock Bajo " + Integer.toString(produ.cantidad)+"\n";
        } else {
            return "Stock en orden con: " + Integer.toString(produ.cantidad)+"\n";
        }
    }
}