import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

class Ventana extends JFrame implements ActionListener {

    private Container contenedor;
    private JLabel titulo;
    private JLabel lbl_duracion;
    private JTextField duracion;
    private JLabel lbl_stock;
    private JTextField stock;
    private JLabel lbl_punto;
    private JTextField punto;
    private JLabel lbl_consumo;
    private JTextField minimo;
    private JTextField maximo;
    private JLabel lbl_tiempo;
    private JTextField dias_1;
    private JTextField dias_2;
    private JButton iniciar;
    private JButton limpiar;
    private JTextArea output;
    private JLabel lbl_errors;
    private Simulador simulador;

    public Ventana() {
        setTitle("Simulador");
        setBounds(300, 90, 900, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        contenedor = getContentPane();
        contenedor.setLayout(null);

        titulo = new JLabel("Simulador de Inventarios");
        titulo.setFont(new Font("Arial", Font.PLAIN, 30));
        titulo.setSize(300, 30);
        titulo.setLocation(300, 30);

        lbl_duracion = new JLabel("Duracion");
        lbl_duracion.setFont(new Font("Arial", Font.PLAIN, 20));
        lbl_duracion.setSize(100, 20);
        lbl_duracion.setLocation(50, 100);

        duracion = new JTextField();
        duracion.setFont(new Font("Arial", Font.PLAIN, 15));
        duracion.setSize(190, 20);
        duracion.setLocation(200, 100);

        lbl_stock = new JLabel("Inventario");
        lbl_stock.setFont(new Font("Arial", Font.PLAIN, 20));
        lbl_stock.setSize(100, 20);
        lbl_stock.setLocation(50, 150);

        stock = new JTextField();
        stock.setFont(new Font("Arial", Font.PLAIN, 15));
        stock.setSize(150, 20);
        stock.setLocation(200, 150);

        lbl_punto = new JLabel("Minimo");
        lbl_punto.setFont(new Font("Arial", Font.PLAIN, 20));
        lbl_punto.setSize(100, 20);
        lbl_punto.setLocation(50, 200);

        punto = new JTextField();
        punto.setFont(new Font("Arial", Font.PLAIN, 15));
        punto.setSize(150, 20);
        punto.setLocation(200, 200);

        lbl_consumo = new JLabel("Consumo");
        lbl_consumo.setFont(new Font("Arial", Font.PLAIN, 20));
        lbl_consumo.setSize(100, 20);
        lbl_consumo.setLocation(50, 250);

        minimo = new JTextField();
        minimo.setFont(new Font("Arial", Font.PLAIN, 15));
        minimo.setSize(50, 20);
        minimo.setLocation(200, 250);

        maximo = new JTextField();
        maximo.setFont(new Font("Arial", Font.PLAIN, 15));
        maximo.setSize(50, 20);
        maximo.setLocation(250, 250);

        lbl_tiempo = new JLabel("Tiempo");
        lbl_tiempo.setFont(new Font("Arial", Font.PLAIN, 20));
        lbl_tiempo.setSize(100, 20);
        lbl_tiempo.setLocation(50, 300);

        dias_1 = new JTextField();
        dias_1.setFont(new Font("Arial", Font.PLAIN, 15));
        dias_1.setSize(50, 20);
        dias_1.setLocation(200, 300);

        dias_2 = new JTextField();
        dias_2.setFont(new Font("Arial", Font.PLAIN, 15));
        dias_2.setSize(50, 20);
        dias_2.setLocation(250, 300);

        iniciar = new JButton("Iniciar");
        iniciar.setFont(new Font("Arial", Font.PLAIN, 15));
        iniciar.setSize(100, 20);
        iniciar.setLocation(150, 450);
        iniciar.addActionListener(this);

        limpiar = new JButton("Limpiar");
        limpiar.setFont(new Font("Arial", Font.PLAIN, 15));
        limpiar.setSize(100, 20);
        limpiar.setLocation(270, 450);
        limpiar.addActionListener(this);

        output = new JTextArea();
        output.setFont(new Font("Arial", Font.PLAIN, 15));
        output.setSize(300, 400);
        output.setLocation(500, 100);
        output.setLineWrap(true);
        output.setEditable(false);

        lbl_errors = new JLabel("");
        lbl_errors.setFont(new Font("Arial", Font.PLAIN, 20));
        lbl_errors.setSize(500, 25);
        lbl_errors.setLocation(100, 500);

        contenedor.add(titulo);
        contenedor.add(lbl_duracion);
        contenedor.add(duracion);
        contenedor.add(lbl_stock);
        contenedor.add(stock);
        contenedor.add(lbl_punto);
        contenedor.add(punto);
        contenedor.add(lbl_consumo);
        contenedor.add(minimo);
        contenedor.add(maximo);
        contenedor.add(lbl_tiempo);
        contenedor.add(dias_1);
        contenedor.add(dias_2);
        contenedor.add(iniciar);
        contenedor.add(limpiar);
        contenedor.add(output);
        contenedor.add(lbl_errors);

        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) { // Detecta los botones
        if (e.getSource() == iniciar) {
            String data = "";
            int[] x_y;

            int[] time = {Integer.parseInt(dias_1.getText()), Integer.parseInt(dias_2.getText())};
            int[] consumo = {Integer.parseInt(minimo.getText()), Integer.parseInt(maximo.getText())};
            simulador = new Simulador(Integer.parseInt(stock.getText()), Integer.parseInt(punto.getText()), consumo, time, Integer.parseInt(duracion.getText()));
            simulador.loop();
            data = simulador.data;
            x_y = simulador.x_y;
            output.setText(data);
            output.setEditable(false);
            lbl_errors.setText("Simulacion Exitosa!");

            XYSeries series = new XYSeries("Ventas");

            for (int i = 1; i < x_y.length; i++) {
                series.add(i, x_y[i]);
            }

            XYSeriesCollection dataset = new XYSeriesCollection(); //guarda datos para los graficos
            dataset.addSeries(series); //  agrgar oSeries que trae los datos
            JFreeChart chart = ChartFactory.createXYLineChart("Ventas de Electronics", "Tiempo", "Cantidad", dataset, PlotOrientation.HORIZONTAL, true, false, false);

            ChartPanel panel = new ChartPanel(chart);    //crear panel 

            output.setLayout(new java.awt.BorderLayout());
            output.add(panel);
            output.validate();
        } else if (e.getSource() == limpiar) {
            String def = "";
            duracion.setText(def);
            stock.setText(def);
            minimo.setText(def);
            maximo.setText(def);
            output.setText(def);
            lbl_errors.setText(def);
            dias_1.setText(def);
            dias_2.setText(def);
            punto.setText(def);
        }
    }
}

class Driver {
    public static void main(String[] args) throws Exception {
        Ventana ventana = new Ventana();
    }
}